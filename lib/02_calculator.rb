def add(num1, num2)
  num1 + num2
end

def subtract(num1, num2)
  num1 - num2
end

def sum(array_of_numbers)
  array_of_numbers.reduce(0, :+)
end

def multiply(num1, num2)
  num1 * num2
end

def power(num1, num2)
  num1 ** num2
end

def factorial(num)
  (1..num).reduce(1) {|accum, ele| accum * ele}
end
