def echo(string)
  string
end

def shout(string)
  string.upcase
end

def repeat(string, num = 2)
  result = string
  (num - 1).times do
    result = result + " " + string
  end
  result
end

def start_of_word(word, num = 1)
  result = ""
  word.chars.each do |letter|
    result << letter if result.length < num
  end
  result
end

def first_word(sentence)
  words = sentence.split(" ")
  words[0]
end

def titleize(title)
  words = title.split(" ")
  result = first_word(title).capitalize
  little_words = %w(a the and if by for in out of to over under through)
  words.each_with_index do |word, index|
    next if index == 0
    little_words
    .include?(word) ? result << " " + word : result << " " + word.capitalize
  end
  result
end
