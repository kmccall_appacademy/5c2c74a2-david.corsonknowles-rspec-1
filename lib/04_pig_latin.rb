def start_of_word(word, num = 1)
  result = ""
  word.chars.each do |letter|
    result << letter if result.length < num
  end
  result
end

def end_of_word(string, num = -1)
  arr = string.split("")
  answer = arr[n..-1]
  answer.join("")
end

def vowel?(letter)
  vowels = ["a", "e", "i", "o", "u", "A", "E", "I", "O", "U"]
  vowels.include?(letter)
end

def vowel_case(word)
  "#{word}ay"
end

def index_of_first_vowel(word)
  # returns the index of the first vowel found
  word.chars.each_with_index do |letter, index|
    return index if vowel?(letter)
  end
  nil
end

def consonant_case(word)
  #word[(index_of_first_vowel(word))..-1]
  # + start_of_word(word, (index_of_first_vowel(word))) + "ay"
  start = "#{word[index_of_first_vowel(word)..-1]}"
  finish = "#{word[0...index_of_first_vowel(word)]}ay"
  start + finish
end

def qu?(word)
 word[index_of_first_vowel(word)] == "u" \
 && word[(index_of_first_vowel(word)-1)] == "q"
end

def phoneme_case(word)
  start = "#{word[(index_of_first_vowel(word)+1)..-1]}"
  finish = "#{word[0..index_of_first_vowel(word)]}ay"
  start << finish
end

def pig_latin(sentence)
  words = sentence.split
  words.each_with_index do |word, index|
    if vowel?(start_of_word(word))
      words[index] = vowel_case(word)
    elsif qu?(word)
      words[index] = phoneme_case(word)
    else
      words[index] = consonant_case(word)
    end
  end
  words.join(" ")
end

def record_capitals(sentence)
  array_of_capitals = []
  words = sentence.split
  words.each_with_index do |word, index|
    array_of_capitals << index if word[0] == word[0].capitalize
  end
  array_of_capitals
end

def downcase_words(sentence)
  words = sentence.split
  words.each_with_index do |w, index|
    words[index] = (w[0].downcase + w[1..-1]) if w[0] == w[0].capitalize
  end
  words.join(" ")
end

def upcase_words(sentence, array)
  words = sentence.split
  words.each_with_index do |word, index|
    words[index] = word.capitalize if array.include?(index)
  end
  words.join(" ")
end

def translate(sentence)
  capitals_array = record_capitals(sentence)
  punctuation_array = record_punctuation(sentence)
  result = downcase_words(sentence)
  result = remove_punctuation(result)
  result = pig_latin(result)
  result = upcase_words(result, capitals_array)
  result = add_punctuation(result, punctuation_array)
end

def remove_punctuation(sentence)
  words = sentence.split
  words.each_with_index do |word, index|
    if has_punctuation?(word)
      words[index] = word[0..-2]
    end
  end
  words.join(" ")
end

def add_punctuation(sentence, array)
  words = sentence.split
  array.each do |punctuation_place|
    words[punctuation_place[0]] = \
    words[punctuation_place[0]] + punctuation_place[1]
  end
  words.join(" ")
end

def is_punctuation?(letter)
  symbols = "!?.,;:"
  symbols.include?(letter)
end

def has_punctuation?(word)
  is_punctuation?(word[-1])
end

def record_punctuation(sentence)
  #returns the ordinal place of the word in the sentence and the punctuation
  result = []
  words = sentence.split
  words.each_with_index do |word, index|
    result << [(index), (word[-1])] if has_punctuation?(word)
  end
  result
end
